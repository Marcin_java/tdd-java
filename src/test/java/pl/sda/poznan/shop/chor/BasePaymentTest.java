package pl.sda.poznan.shop.chor;

import static org.junit.Assert.*;

import org.junit.Test;

public class BasePaymentTest {

  @Test
  public void pay() {
    Account account = new Account();
    account.setBalance(1000D);
    Payment paypass = new PaypassPayment(account);
    Payment pinPayment = new PinPayment(account);

    paypass.setNextHandler(pinPayment);

    boolean isSuccess = paypass.handle(2000D);
    boolean isSuccesWithLowAmount = paypass.handle(200D);

    assertEquals(false, isSuccess);
    assertEquals(true, isSuccesWithLowAmount);
  }
}